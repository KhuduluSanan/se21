package se21;

public class MinimumDistances {
//      String[] alphabet = {"a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w x, y, z"};

    public static void main(String[] args) {
        int[] arr = {7, 1, 3, 4, 1, 7};
        int minimum = arr.length;

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    int indexFerqi = j - i;

                    if (indexFerqi <= minimum) {
                        minimum = indexFerqi;
                    }
                }
            }
        }


        if (minimum == arr.length){
            System.out.println(-1);
        }
        else {
            System.out.println(minimum);
        }
    }
}