package se21;

public class DrawinkBook {
    public static void main(String[] args) {
        System.out.println(page(8,6));
    }

    static int page(int book, int find) {
        int firstFront = find / 2;
        int firstBack = (book - find) / 2;
        return Math.min(firstFront, firstBack);
    }

}
