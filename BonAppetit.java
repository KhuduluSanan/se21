package se21;

import java.util.Scanner;

public class BonAppetit {

    public static void main(String[] args) {

        int[] eatCost = {3, 10, 2, 9};
        int friendsPay = 12; // Dostlarin birlikde odediyi pul
        int sum = 0;
        int overPaid = eatCost[1]; //Artıq odenilmis yemek

        for (int i = 0; i < eatCost.length; i++) {
            if (eatCost[i] ==eatCost[1]){
                continue;
            }
            sum +=eatCost[i]; //Yemeklerin qiymetler cemi. 1ci xaric.
        }
        int willPay = sum - overPaid; //odenilecek mebleq willPay 4azn
        int resultPay = (friendsPay/2) - willPay;

        if (resultPay == 0){
            System.out.println("Bon Appetit");
        }
        else {
            System.out.println(resultPay);
        }

    }
}
